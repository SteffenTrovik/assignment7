# Assignment 4 - Pokémon Trainer using Angular
In this assignment we have a created a dynamic web application which takes a user through a login process and allows them to catch and display their caught pokemons.

Pokemon Trainer Application can be accessed here:
[Pokemon Trainer API](https://sat-noroff-api.herokuapp.com/trainers)

## Development Environment
For our Pokemon Trainer application we've used the following software:
* Figma to design our component tree
* Visual Studio Code (Jone) & WebStorm (Steffen) as integrated development environments
* Postman to test application communication with APIs
* Angular Browser DevTool for testing and debugging
* Git to merge code
* PokeAPI to get images and stats of pokemons
* Trainer API to register/login user and register caught pokemons
* Heroku to deploy website

## Component Tree

![Component Tree](component-tree.png)


## Landing Page
The landing page of our application features a register/login interface in which the user can enter a username. Our application will then check in our trainer api whether the user is already registered, and if so, log the user into our pokemon trainer application. If the user is not already registered, we will create a user and log it in.

## Trainer Page
Our trainer page contains which pokemons have already been caught by the trainer. We display the name of the pokemon caught and show an image of that specific pokemon in a pokemon card style view alongside a button to show stats and a button to release specific pokemons from the trainer. The user can navigate to the catalogue page from the bar at the top of the page where the user can catch pokemons.

## Catalogue Page
The catalogue page allows the user to catch pokemons by hovering certain cards and clicking on the pokemons they would like to catch. When hovering a specific pokemon card, the card changes it's background image and mouse cursor in order to let the user know they're hovering a pokemon card. Our catalogue page loads all the pokemons from the API and stores them in a session. If the user reloads the page, it will not fetch the pokemons from the API, but will instead get them from the session storage. We do this to limit requests to the pokemon API. Each page showcases a total of 20 pokemons each. The user can navigate between different pokemon pages by clicking the "previous" or "next" buttons, which will take the user to whatever page they would like to go to.

## Contributers

* Steffen Trovik
* Jone Jeremiassen
