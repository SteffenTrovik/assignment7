import { Component, OnInit } from "@angular/core";
import { Pokemon } from "../models/pokemon.model";
import { PokemonService } from "../services/pokemon.service";
import { SessionService } from "../services/session.service";

@Component({
  selector: 'app-catalogue',
  templateUrl: 'catalogue.component.html',
  styleUrls: ['catalogue.component.css']
})

export class CatalogueComponent implements OnInit {
  public displayedPokemon: Pokemon[] = []
  public caught: boolean = false;
  public index: number = 0;

  private API_URL: string = "https://pokeapi.co/api/v2/pokemon";

  constructor(
    private readonly pokemonService: PokemonService,
    private readonly sessionService: SessionService
  ) {}

  //On initialization, grab all the pokemons from the API through the pokemonservice .getPokemons method
  ngOnInit(): void {
    this.pokemonService.getPokemons(`${this.API_URL}?limit=1118`,)
  }

  //Method used to decrease the index when clicking the "Previous" button on the catalogue page
  //This basically allows us to change the pokemon view of the previous page
  public decreaseIndex() {
    if (this.index >= 20)
      this.index -= 20
  }

  //Method to increase index by 20 when the "Next" button on catalogue page is pressed to show next pokemons on the next page
  public increaseIndex() {
    if (this.index <= this.sessionService.getPokemon().length)
      this.index += 20
  }

  //When a pokemon is clicked on the catalogue page, we want to catch it.
  //This basically calls the addPokemon method from the sessionService which basically adds a pokemon caught to the storage.
  public catch(pokemon: Pokemon) {
    if (this.sessionService.getTrainerPokemon().length >= 20) {
      window.alert("You can have maximum 20 pokemon, release some pokemon to catch more!")
    } else {
      this.sessionService.addPokemon(pokemon)
    }
  }

  showStats(pokemon: Pokemon) {

  }

  //Gets 20 pokemons depending on their index and adds the caught pokemon ball to the ones caught by the trainer (user)
  get pokemons(): Pokemon[] {
    this.displayedPokemon = this.sessionService.getPokemon().slice(this.index, this.index + 20)
    for (const p of this.displayedPokemon) {
      p.caught = false
      for (const tp of this.sessionService.getTrainerPokemon()) {
        p.stats = {}
        if (p.id === tp.id) {
          p.caught = true
          break
        }
      }
    }
    return this.displayedPokemon
  }
}
