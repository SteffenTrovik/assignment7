import { Component } from "@angular/core";

@Component({
  selector: 'app-container',
  template: `
    <div class="container">
      <ng-content></ng-content>
    </div>
  `,
  styles:[
    `.container {
			max-width: 100rem;
			width: 100%;
			padding: 0 1.5rem;
			margin: 0 auto;

			width: 1400px;
			height: 1700px;
			background-image: url("https://wallpapercave.com/wp/wp2098819.jpg");
			background-repeat: repeat;
		}`
  ]
})
export class ContainerComponent {}
