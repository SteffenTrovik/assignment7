import {Component, OnInit} from '@angular/core'
import {Pokemon, Trainer} from "../models/pokemon.model";
import {SessionService} from "../services/session.service";

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['trainer.component.css']
})

export class TrainerComponent implements OnInit{
  constructor(private readonly sessionService: SessionService){

  }

  //Logout trainer calls logout method in session service
  public logout(){
    this.sessionService.logout()
  }

  //Release pokemon from trainer collection, invoke removePokemon in session service
  release(pokemon: Pokemon){
    this.sessionService.removePokemon(pokemon)
  }

  ngOnInit(): void {
    //this.loginService.fetchTrainer();
  }

  //Getter for trainer, gets trainer from session
  get trainer(): Trainer{
    return <Trainer>this.sessionService.getTrainer()
  }

}