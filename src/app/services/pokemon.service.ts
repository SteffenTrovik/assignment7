import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Pokemon } from "../models/pokemon.model";
import { SessionService } from "./session.service";


@Injectable({
  providedIn: 'root'
})

export class PokemonService {
  private _pokemons: Pokemon[] = [];
  private _error: string = "";


  constructor(
    private readonly http: HttpClient,
    private readonly sessionService: SessionService
  ) { }

  //Gets all the pokemons from the pokemon api
  public getAllPokemon(url: string): Observable<any> {
    return this.http.get<any>(url);
  }


  //Checks if pokemons already in local storage, if so grabs from storage, else from api
  public getPokemons(url: string): void {
    const storedPokemon = localStorage.getItem('pokemon')
    if (storedPokemon) {
      this._pokemons = JSON.parse(storedPokemon);
      this.sessionService.setPokemon(this._pokemons)
    } else {
      this.getAllPokemon(url).subscribe(
        (pokemon: any) => {
          if (pokemon) {
            this.sessionService.setPokemon(pokemon.results);
            this.sessionService.setNext(pokemon.next);
          }
        },
        (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      );
    }
  }

  //Pokemons getter
  public pokemons(): Pokemon[] {
    return this._pokemons;
  }

  //Error getter
  public error(): string {
    return this._error;
  }
}

