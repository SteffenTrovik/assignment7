import { Injectable } from "@angular/core";
import { Pokemon, Trainer } from "../models/pokemon.model";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Router } from "@angular/router";

const API_URL = environment.apiBaseUrl;
const apiKey = environment.apiKey;

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private _trainer: Trainer | undefined;
  private _pokemon: Pokemon[] = [];
  private _error: string = '';

  constructor(private readonly http: HttpClient, private readonly router: Router,) {
    const storedTrainer = localStorage.getItem('trainer')
    if (storedTrainer) {
      this._trainer = JSON.parse(storedTrainer) as Trainer;
    }
  }

  //Getter for trainer
  getTrainer(): Trainer | undefined {
    return this._trainer;
  }

  //Sets trainer and sets the local storage for trainer
  setTrainer(trainer: Trainer): void {
    this._trainer = trainer;
    localStorage.setItem('trainer', JSON.stringify(trainer))
  }

  //Getter for pokemon
  getPokemon() {
    return this._pokemon
  }

  //Adds a pokemon to a trainer, adds it to local storage and updates the trainer in the API db
  public addPokemon(pokemon: Pokemon) {
    this._trainer?.pokemon.push(pokemon)
    localStorage.setItem('trainer', JSON.stringify(this._trainer))

    this.updateTrainer()
  }

  //Removes a specific pokemon from a trainer, updates local storage and API db
  public removePokemon(pokemon: Pokemon) {
    const index = this._trainer?.pokemon.indexOf(pokemon)
    // @ts-ignore
    if (index > -1) {
      // @ts-ignore
      this._trainer?.pokemon.splice(index, 1);
    }
    localStorage.setItem('trainer', JSON.stringify(this._trainer))
    this.updateTrainer()
  }

  //Method to update a specific trainer in the API db
  private updateTrainer() {
    this.http.patch(`${API_URL}/trainers/${this._trainer?.id}`,
      { pokemon: this._trainer?.pokemon },
      {
        headers: {
          'X-API-Key': apiKey,
          'Content-Type': 'application/json'
        }
      })
      .subscribe(
        () => {

        },
        (error: HttpErrorResponse) => {
          this._error = error.message;
          console.log(this._error)
        }
      )
  }


  //Getter for trainer pokemon
  getTrainerPokemon() {
    // @ts-ignore
    return this.getTrainer().pokemon;
  }

  //Sets the avatar and ID of pokemons and updates local storage
  setPokemon(pokemon: any): void {
    for (const p of pokemon) {
      let url = p.url.split("/")
      let id = url[url.length - 2]
      p.avatar = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/' + id + '.png'
      p.id = id
      this._pokemon.push(p)
    }
    localStorage.setItem('pokemon', JSON.stringify(pokemon));
  }

  //Index handler for the next page in catalogue in local storage
  setNext(next: string): void {
    localStorage.setItem('next', next);
  }

  //Gets which pokemons to show in catalogue
  getNext(): string {
    return localStorage.getItem('next') || '';
  }

  //Logout method, removes trainer string in local storage and redirects user to login page
  logout() {
    this._trainer = undefined
    localStorage.removeItem('trainer')
    this.router.navigate(['login'])
  }
}
