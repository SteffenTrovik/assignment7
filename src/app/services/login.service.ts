import { Injectable } from '@angular/core'
import { environment } from "../../environments/environment";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Trainer } from "../models/pokemon.model";
import { Observable, of, throwError } from "rxjs";
import { catchError, finalize, retry, switchMap, tap } from "rxjs/operators";
import { SessionService } from "./session.service";

const API_URL = environment.apiBaseUrl;
const apiKey = environment.apiKey;

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private _trainer: Trainer = { id: "", pokemon: [], username: "" };
  public attempting: boolean = false;
  private _error: string = '';



  constructor(
    private readonly http: HttpClient,
    private readonly SessionService: SessionService) {
  }

  //Fetches a specific trainer from the API db by username
  public fetchTrainer(username: string): Observable<Trainer[]> {
    return this.http.get<Trainer[]>(`${API_URL}/trainers?username=${username}`)
  }


  //Creates a trainer in the trainers API db
  public createTrainer(username: string): Observable<Trainer> {
    return this.http.post<Trainer>(`${API_URL}/trainers`,
      { username, pokemon: [] },
      {
        headers: {
          'X-API-Key': apiKey,
          //'Content-Type': 'application/json'
        }
      })
  }


  //Attempts to authenticate a user login.
  public authenticate(username: string, onsuccess: () => void): void {
    //Sets attmpting to true while attempting to login
    this.attempting = true

    //Upon error, throw error message
    const catchRequestError = (trainer: Trainer) => {
      return throwError('Could not create a user.');
    }

    //Trainer to session
    const tapToSession = (trainer: Trainer) => {
      this.SessionService.setTrainer(trainer)
    }


    //Checks if username is already registered in API db, log user in if so, else register username and login
    this.fetchTrainer(username)
      .pipe(
        retry(3),
        switchMap((trainers: Trainer[]) => {
          if (trainers.length) {
            return of(trainers[0])
          }
          return this.createTrainer(username)
        }),
        catchError(catchRequestError),
        tap(tapToSession),
        finalize(() => {
          this.attempting = false;
        })
      )
      .subscribe(
        (trainer: Trainer) => {
          if (trainer.id) {
            //setTrainer
            this._trainer = trainer
            onsuccess();
          }
        },
        (error: HttpErrorResponse) => {
          this._error = error.message
        }
      )
  }


  public updateTrainer(): void {
    let trainer = this.SessionService.getTrainer()
    // @ts-ignore
    this.http.patch<Trainer>(`${API_URL}/${trainer.id}`,
      { pokemon: trainer?.pokemon },
      {
        headers: {
          'X-API-Key': apiKey,
          //'Content-Type': 'application/json'
        }
      })
  }





  public trainer(): Trainer {
    return this._trainer
  }

  public error(): string {
    return this._error
  }

}
