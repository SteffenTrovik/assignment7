import { RouterModule, Routes } from "@angular/router";
import { LandingComponent } from "./landing/landing.component";
import { CatalogueComponent } from "./catalogue/catalogue.component";
import { TrainerComponent } from "./trainer/trainer.component";
import { NgModule } from "@angular/core";
import { AuthGuard } from "./services/auth.guard";


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  },
  {
    path: 'login',
    component: LandingComponent
  },
  {
    path: 'catalogue',
    component: CatalogueComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'trainer',
    component: TrainerComponent,
    canActivate: [AuthGuard]
  }

]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}