import {Component, OnInit} from '@angular/core'
import {LoginService} from "../services/login.service";
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {SessionService} from "../services/session.service";

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['landing.component.css']
})

export class LandingComponent{
  constructor(
    private readonly loginService: LoginService,
    private readonly router: Router,
    private readonly sessionService: SessionService,
  ) {}

  //Gets the value of the attempting bool variable in the login service
  get attempting(): boolean {
    return this.loginService.attempting;
  }

  //Login page on submit method, attempts to authenticate the user to the webpage and redirect it to the trainer page
  onSubmit(loginForm: NgForm): void {
    this.loginService.authenticate(loginForm.value.trainer, async () => {
      await this.router.navigate(['trainer'])
    })
  }


  //If there is already a trainer set in session, navigate user to trainer page instead of trying to go to log in page
  ngOnInit(): void {
    if (this.sessionService.getTrainer() !== undefined) {
      this.router.navigate(['trainer'])
    }
  }
}
