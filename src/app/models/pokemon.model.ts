
export interface Trainer {
  id: '',
  username: '',
  pokemon: Pokemon[]
}

export interface Pokemon {
  stats: {};
  caught: boolean;
  id: number;
  name: string;
  url: string;
  avatar: string
}
